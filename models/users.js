// cretion de shemas de table users
const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({

    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    pseudo: {
        type: String,
        required: false
    },
    admin: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now
    },


})

const User = mongoose.model('user', userSchema);
module.exports = User;