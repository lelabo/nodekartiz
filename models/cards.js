// cretion de shemas de table cards

const mongoose = require('mongoose');
const cardSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    style: {
        type: String,

        default: ""
    },
    template: {
        type: String,

        default: ""
    },
    dataArray: {
        type: [Object],

        default: ""
    },
    userId: {
        type: String,

    },
    date: {
        type: Date,
        default: Date.now
    }


})

const Card = mongoose.model('card', cardSchema);
module.exports = Card;