const chai = require('chai');
const expect = chai.expect;
const should = chai.should();
const chaiHttp = require('chai-http');
const server = require('../server.js');
const Cards = require('../models/cards');


chai.use(chaiHttp);

describe('Test', () => {

    it('test ...', (done) => {

        chai.request(server)
            .get('/welcome')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                const actualVal = res.body.message;
                expect(actualVal).to.be.equal('Welcome');
                done();
            });
    });
    it('should verify that authentication exigent', (done) => {
        chai.request(server)
            .get('/cards/')
            .end((err, res) => {
                res.should.have.status(401);
                // status 401 Une authentification est nécessaire pour accéder à la ressource.
                done();

            });
    })


    it('should register + login a user, create card and verify 1 in DB', (done) => {

        // 1) Register new user
        let user = {
            pseudo: "Peter Petersen",
            email: "mail000@petersen.com",
            password: "123456",
        }
        chai.request(server)
            .post('/auth/register')
            .send(user)
            .end((err, res) => {

                // Asserts
                expect(res.status).to.be.equal(201);
                expect(res.body).to.be.a('object');
                expect(res.body.error).to.be.equal(undefined);

                // 2) Login the user
                chai.request(server)
                    .post('/auth/login')
                    .send({
                        "email": "mail000@petersen.com",
                        "password": "123456"
                    })
                    .end((err, res) => {
                        // Asserts   
                        expect(res.status).to.be.equal(200);
                        expect(res.body.error).to.be.equal(undefined);

                        let token = res.body.accessToken;
                        let userId = res.body.id

                        // 3) Create new product
                        let card =
                        {
                            name: "Name",
                            template: "<div>template {{ name }}</div>",
                            style: "color:red",
                            dataArray: [{ "name": "Peter" }, { "nameToo": "Peterson" }],
                            userId: userId,
                        };

                        chai.request(server)
                            .post('/cards/register')
                            .set({ "auth-token": token })
                            .send(card)
                            .end((err, res) => {

                                // Asserts
                                expect(res.status).to.be.equal(201);
                                expect(res.body.message).to.be.equal("Card created successfully!")
                                expect(res.body.data.template).to.be.equal(card.template)
                                expect(res.body.data.style).to.be.equal(card.style)
                                expect(res.body.data.dataArray).to.be.a('array')

                                let cardId = res.body.data._id
                                // 4) Verify one product in test DB
                                chai.request(server)
                                    .get('/cards')
                                    .set({ "auth-token": token })
                                    .end((err, res) => {

                                        // Asserts

                                        expect(res.status).to.be.equal(200);
                                        expect(res.body).to.be.a('array');
                                        expect(res.body.length).to.be.above(1);

                                    });

                                // delete card
                                chai.request(server)
                                    .delete(`/cards/delete/${cardId}`)
                                    .set({ "auth-token": token })
                                    .end((err, res) => {

                                        // Asserts


                                        expect(res.status).to.be.equal(200);
                                        expect(res.body.message).to.be.equal("Card delete successfully!")

                                        // 4) Verify one product in test DB
                                        chai.request(server)
                                            .get('/cards')
                                            .set({ "auth-token": token })
                                            .end((err, res) => {

                                                // Asserts

                                                expect(res.status).to.be.equal(200);
                                                expect(res.body).to.be.a('array');
                                                expect(res.body.length).to.be.above(1);

                                            });

                                        //delete user
                                        chai.request(server)
                                            .delete(`/auth/delete/${userId}`)
                                            .set({ "auth-token": token })
                                            .end((err, res) => {

                                                // Asserts
                                                console.log(userId)
                                                expect(res.status).to.be.equal(200);
                                                expect(res.body.message).to.be.equal('User delete successfully!')
                                                // expect(res.body.length).to.be.above(1);
                                                done();
                                            }).timeout(10000);

                                    });
                            });
                    });
            });
    });

})
