const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const postRoutes = require('./routes/posts')
const app = express();
const cardRoutes = require('./routes/cards')
const authRoutes = require('./routes/auth')
const forgotRoutes = require('./routes/auth')
dotenv.config();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Connection a la base donné
const mongoDB = process.env.DB_CONNECT
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
  console.log('connecte a la basa done')
}).catch(err => console.log(err));

//
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
 
  //res.setHeader('Vary', 'Origin');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization, auth-token');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

// routes Middlewere
app.use('/cards', cardRoutes)
app.use('/auth', authRoutes)
app.use('/posts', postRoutes)
app.use('/auth', forgotRoutes)






module.exports = app;