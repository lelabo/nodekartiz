const router = require('express').Router();
const User = require('../models/users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const jwt_decode = require('jwt-decode');
const nodemailer = require("nodemailer");
const { token } = require('morgan');
const verify = require('./verifyToken');
const Joi = require('@hapi/joi');

const schema = Joi.object({
  email: Joi.string().min(6).required().email(),
  password: Joi.string().min(6).required(),
  pseudo: Joi.string().required(),

})

// singup
router.post('/register', async (req, res) => {
  const { error } = schema.validate(req.body);

  if (error) {
    return res.status(400).send(error.details[0].message)
  }
  //cheking if the user is already in the database
  const emailExist = await User.findOne({ email: req.body.email });
  if (emailExist) {
    return res.status(400).send('Email already exist');
  }

  //crpting password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  //New User
  const user = new User({
    email: req.body.email,
    password: hashedPassword,
    pseudo: req.body.pseudo,
    admin: req.body.admin,
  });
  try {
    const saveUser = await user.save()
      .then(() => {
        res.status(201).json({
          message: 'User saved successfully!'
        });
      });
  } catch (error) {
    res.status(400).json({
      error: error
    })
  }
});

//Login
router.post('/login', async (req, res) => {
  // checkig if the email exists
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    return res.status(400).json({
      message: 'Email not found!'
    });
  }
  // checkig if the password is valid
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.status(400).json({
    message: 'Invalid password'
  })

  // create and assing a token
  const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET, { expiresIn: '43200s' })
  res.header('auth-token', token)
    .send({
      id: user._id,
      pseudo: user.pseudo,
      email: user.email,
      accessToken: token
    })
})

// create a routes of forget and reset password
router.post('/forgot', async (req, res) => {
  // checkig if the email exists
  const user = await User.findOne({ email: req.body.email })
  console.log(user.email)
  if (user) {

    const update = User.findByIdAndUpdate({ _id: user._id }, { tokenReset: token, reset_password_expires: Date.now() + 86400000 }, { new: true })
    const userUpdateId = update._conditions._id
    console.log(userUpdateId);
    const tokenReset = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET, { expiresIn: '1200s' })
    res.header('auth-token', tokenReset)
    const payload = { user, tokenReset }
    console.log(payload)
    //send email avec gmail
    const sendEmail = async (email, subject, payload, template) => {
      try {
        // create reusable transporter object
        const transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: process.env.EMAIL_USERNAME,
            pass: process.env.EMAIL_PASSWORD,
          },
        });

        const options = {
          from: 'org.kartiz@gmail.com',
          to: email,
          subject: 'Account Activation Link',
          html: ' http://localhost:3000/resetPassword?token=' + tokenReset


        };
        // Send email
        transporter.sendMail(options, function (error, info) {
          if (error) {
            return console.log(error);
          }
          console.log('Message sent: ' + info.response + Date());
        });
      } catch (error) {
        return error;
      }

    };
    sendEmail(
      user.email
    )
    return res.status(201).send({
      message: 'Check your email-box',

    })
  } else {
    return res.status(400).send({
      message: 'Email not found!'
    });
  }


});
// Update password
router.post('/reset', async (req, res) => {
  //decoding token
  var decoded = jwt_decode(req.body.token)

  //coding inpyt password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  //update password
  User.findOneAndUpdate({ _id: decoded._id }, { $set: { password: hashedPassword } })
    .then(() => res.status(202).json({
      message: "Password changed accepted, you can login"
    }))
    .catch(err => res.status(500).json(err))
})

router.delete('/delete/:id', (req, res) => {
  User.deleteOne({ _id: req.params.id })
    .then(() => {
      res.status(200).json({ message: 'User delete successfully!' });
    })
        .catch((error) => {
          res.status(400).json({ error: error });
        });
    });


module.exports = router;