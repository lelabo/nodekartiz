const router = require('express').Router();
const Card = require('../models/cards')
const verify = require('./verifyToken');

//creating card
router.post('/register', (req, res, next) => {
  delete req.aborted._id;
  const card = new Card({
    name: req.body.name,
    template: req.body.template,
    style: req.body.style,
    dataArray: req.body.dataArray,
    userId: req.body.userId,
  });
  card.save()

    .then(data => { res.status(201).send({data, "message":"Card created successfully!"})})
    .catch(err => { res.status(500).send({ message: err.message }); 
});

});

//update
router.put('/update/:id', verify, (req, res, next) => {

  Card.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
    .then(() => {
      res.status(200).json({ message: 'Card update successfully!' });
    }).catch((error) => {
      res.status(400).json({ error: error });
    });
});


//delete
router.delete('/delete/:id', verify, (req, res, next) => {
  Card.deleteOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
    .then(() => {
      res.status(200).json({ message: 'Card delete successfully!' });
    })
    .catch((error) => {
      res.status(400).json({ error: error });
    });
});


// read per id
router.get('/:id', verify, (req, res, next) => {
  Card.findOne({ _id: req.params.id })
    .then((cards) => { res.status(200).json(cards) })
    .catch((error) => {
      res.status(400).json({ error: error });
    });
});

// read all
router.get('/', verify, (req, res, next) => {

  Card.find().then((cards) => { res.status(200).json(cards) })
    .catch((error) => {
      res.status(400).json({ error: error });
    });
});



module.exports = router;