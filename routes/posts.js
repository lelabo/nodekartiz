const router = require('express').Router();
const User = require('../models/users');
const verify = require('./verifyToken');

//route verify token
router.get('/', verify, (req, res) => {
    res.send(req.user);
  
})

module.exports = router;